from rply import LexerGenerator
import sys 
lg = LexerGenerator() 
lg.add('openTag', r'<[a-zA-Z]+>')
lg.add('closeTag',r'</[a-zA-Z]+>')
# lg.add('singleTag',r'</[a-zA-Z]+>')
lg.add('text' ,r'[A-Za-z0-9]+')
# lg.add('pops', r'[href]')
lg.ignore(r'\s+')


l = lg.build() 

input_str = "".join(sys.stdin.readlines())
for token in l.lex(input_str): 
    print(token.value , token.name)