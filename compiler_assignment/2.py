'''
Sir I tried to implement with the help of 'Calc2' file  
and I dont understand how  to implement a 'function' .

'''
import sys
from rply import LexerGenerator ,ParserGenerator 

lg = LexerGenerator() 
lg.add('print', r'print')
lg.add('int',r'int')
lg.add('(', r'\(')
lg.add(')', r'\)')
lg.add('*',r'\*')
lg.add('/',r'\/')
lg.add('^',r'\^')
lg.add('+', r'\+')
lg.add('-', r'\-')
lg.add('num', r'\d+')
lg.add('var',r'[a-z]')
lg.add('=', r'\=') 
lg.add('!',r'\!')

lg.ignore(r'\s+')

dic = {}

l = lg.build() 
pg = ParserGenerator(['num', '(', ')','+', '-','/','*','^','var','=','print','!','int'],
    precedence = [('left',['+','-']),
                  ('left',['*','/']),
                  ('right',['^'])           
                ]
)
@pg.production('prog : prog statement !')
def dec(p) :
    pass
@pg.production('prog : prog printing !') 
def dec_print(p):
    pass
@pg.production('prog :')
def dec_e(p) :
    pass

@pg.production('statement : expression') 
def wdec(p) :
    return p[0]

@pg.production('printing : print expression') 
def printing(p) :
    print (p[1])

@pg.production('statement : int var = expression') 
def decalration(p):
    dic[p[1].value] = p[3]


@pg.production('expression : ( expression )') 
def exp_paren(p) :
    return p[1]

@pg.production('expression : var') 
def exp_var(p) :
    return dic[p[0].value] 

@pg.production('expression : num') 
def exp_num(p) :
    return int(p[0].value)


@pg.production('expression : expression + expression')
@pg.production('expression : expression - expression')
@pg.production('expression : expression * expression')
@pg.production('expression : expression / expression')
@pg.production('expression : expression ^ expression')
def expression_op(p) :
    left = p[0] 
    op = p[1]
    right = p[2] 
    if op.gettokentype() == "+" :
        return left + right
    elif op.gettokentype() == "-" :
        return left - right
    elif op.gettokentype() == "*" :
        return left * right
    elif op.gettokentype() == "/" :
        return left / right 
    elif op.gettokentype() == "^" :
        return left**right

# sample_input = '''int a = 5+2!
#                 int b = a  !
#                 print b !
#                 print a^2^3 ! '''

input_str = "".join(sys.stdin.readlines())
p = pg.build()
p.parse(l.lex(input_str))
