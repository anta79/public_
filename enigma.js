class Rotor {
    constructor(arr) {
        this.data = arr.slice() 
    }
    setEdge(e) {
        this.e = e  
    }
    getEdge() {
        return this.e 
    }

    rotate() {
        let temp = this.data.slice(1) 
        temp.push(this.data[0]) 
        this.data = temp.slice() 
    }

    setTop(e) {
        while(this.getTop() !== e) {
            this.rotate() 
        }
    }
    getTop() {
        return this.data[0][0]
    }

    getLeft(pos) {
        let r = this.data[pos][1] 
        for(let i=0;i<26;i++) {
            if(this.data[i][0] === r) {
                return i ;
            }
        }
    }

    getRight(pos) {
        let r = this.data[pos][0] 
        for(let i=0;i<26;i++) {
            if(this.data[i][1] === r) {
                return i ;
            }
        }
    }

}

class Reflector {
    constructor(arr) {
        this.data = arr.slice() 
    }
    getOther(pos) {
        let ch = this.data[pos]
        for(let i=0;i<26;i++) {
            if(i !== pos && this.data[i] === ch) {
                return i 
            }
        }
    }
}


refl = ['a','b','c','d','e','f','g','d','i','j','k','g','m','k','m','i','e','b','f','t','c','v','v','j','a','t'] 

arr1 = ['e','k','m','f','l','g','d','q','v','z','n','t','o','w','y','h','x','u','s','p','a','i','b','r','c','j'] 
arr2 = ['a','j','d','k','s','i','r','u','x','b','l','h','w','t','m','c','q','g','z','n','p','y','f','v','o','e']
arr3= ['b','d','f','h','j','l','c','p','r','t','x','v','z','n','y','e','i','w','g','a','k','m','u','s','q','o'] 

arr1_ = arr1.map((ele,ind) => ([String.fromCharCode(97+ind),ele]))
arr2_ = arr2.map((ele,ind) => ([String.fromCharCode(97+ind),ele]))
arr3_ = arr3.map((ele,ind) => ([String.fromCharCode(97+ind),ele]))

rtr1 = new Rotor(arr1_)
rtr2 = new Rotor(arr2_)
rtr3 = new Rotor(arr3_)
reflector = new Reflector(refl)

rtr1.setEdge("q")
rtr2.setEdge("e")
rtr3.setEdge("v")



/* initial setUp */

rtr1.setTop("c")
rtr2.setTop("a")
rtr3.setTop("t") 

/*              */ 



let testStr = "enigma"
let result = [] 
for(let ele of testStr) {

    /* rotations of rotors */
    
    let temp1 = rtr3.getTop()
    if(temp1 === rtr3.getEdge()) {
        let temp2 = rtr2.getTop() 
        if(temp2 === rtr2.getEdge()) {
            rt1.rotate()
        }
        rtr2.rotate() 
    }

    rtr3.rotate()
    /*                     */
    let cur = ele.charCodeAt(0) - 97 ;
    a1 = rtr3.getLeft(cur) 
    a2 = rtr2.getLeft(a1)
    a3 = rtr1.getLeft(a2) 
    x = reflector.getOther(a3)
    b1 = rtr1.getRight(x) 
    b2 = rtr2.getRight(b1)
    b3 = rtr3.getRight(b2) 
    result.push(String.fromCharCode(97+b3))
}

let resStr = result.join("") 
console.log(`Encrypted message -------'${resStr}'`)

/* initial setUp */

rtr1.setTop("c")
rtr2.setTop("a")
rtr3.setTop("t") 

/*              */ 

result = [] 
for(let ele of resStr) {

    /* rotations of rotors */
    
    let temp1 = rtr3.getTop()
    if(temp1 === rtr3.getEdge()) {
        let temp2 = rtr2.getTop() 
        if(temp2 === rtr2.getEdge()) {
            rt1.rotate()
        }
        rtr2.rotate() 
    }

    rtr3.rotate()
    /*                     */
    let cur = ele.charCodeAt(0) - 97 ;
    a1 = rtr3.getLeft(cur) 
    a2 = rtr2.getLeft(a1)
    a3 = rtr1.getLeft(a2) 
    x = reflector.getOther(a3)
    b1 = rtr1.getRight(x) 
    b2 = rtr2.getRight(b1)
    b3 = rtr3.getRight(b2) 
    result.push(String.fromCharCode(97+b3))
}

console.log(`Decrypted message -------'${result.join("")}'`)
